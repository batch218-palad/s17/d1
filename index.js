// console.log("Hello World!");

// [SECTION] Functions
//  Functions in Javascript are lines/block of codes that tell our device/application to perform a certain task when  called/invoked
// Are also used to prevent repeating lines/block of codes that perform the same task/function

// Fucntion Declaration
	// function statment defines a function with parameters

	// function keyword - use to define javascript function
	// function name is used so we are able to call/invoke a declared function
	// function code block({}) - the statement which is compromise the body of the function. This is where the code to be executed.

// function declaration // function name
// function name requires open and close parenthesis deside it
function printName(){  // code block
	console.log("My name is John")  //function statement
}; //delimeter

printName(); // Function Invocation


// [HOISTING] 
	// behavior for certain variables and functions to run or use before their declaration
declaredFunction();
// make sure the function is existing whenever we call/invoke a function

	function declaredFunction(){
		console.log("Hello world")
	};

declaredFunction();

// [Function Expression]
	// A function can be also be stored in a variable. 
	// Hoisting cannot be used in Function Expression unlike in function declaraion

// variableFunction();

let variableFunction = function(){
	console.log("Hello again")
};

variableFunction();

let functionExpression = function funcName(){
	console.log("Hello from the other side.")
};

functionExpression();
// funcNmae();
// Error: Whenever we are calling a named function stored in a variable, we just call the variable name, not the function name

console.log("--------");
console.log("Reassigning Functions");

declaredFunction();

declaredFunction = function(){
	console.log("updated declaredFunction.")
};

declaredFunction();

funcExpression = function(){
	console.log("updated funcExpression.");
}

funcExpression();

// Constant function
const constantFunction = function(){
	console.log("Initialize with const.")
};

constantFunction();

// constantFunction = function(){
// 	console.log("New value.")
// };
// constantFunction();
//		function with const cannot be reassigned.
	// scope is accessibility / visibility of a variable in the code

/*
	Javascript variable has 3 types of scope
	1. local / block
	2. global
	3. function
*/

console.log("--------");
console.log("[Function Scoping]");

{
	let localVar = "Armando Perez";
	console.log(localVar);
}
// console.log(localVar);

let globalVar = "Mr. Worldwide";
console.log(globalVar);

{
	console.log(globalVar);
}

// [Funtion Scoping]
	// variables defined inside a funcition are not accessible/vissible outside the function
	// Variables declared with var, let, and const are quite similar when declared inside the function

function showNames(){
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);

}

showNames();

// Error - these are functions scoped variable and cannot be accessed outse the fucntion they were declared
// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);

//  Nested Functions
	// You create another function inside a function, called nested function


function myNewFunction(){
	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John"
		console.log(name);
	}

	nestedFunction();
	console.log(nestedFunction);
}

myNewFunction();



// Global Scoped Variable
let globalName = "Zuitt";

function myNewFunction2(){
	let nameInside = "Renz";
	console.log(globalName)
}

myNewFunction2();

// alert() allows to show a small window to show information to our users

// alert("Sample Alert");

function showSamlpleAlert(){
	alert("Hello, User");
}
showSamlpleAlert();
	// alert messages inside a function will only execute whenever we call/invoke the function

console.log("I will only log in the console when the alert is dismissed"); 

//  NOte: 
	// Show only an alert for short dialogs/messages to the user
	// Do not overuse alert() because the program/js has tp wait for it to be dismissed before continuing

// [Prompt]
	// prompt() allow us to show all window at the top of the browser to gather user input.

	// let samplePrompt = prompt("Enter your Full Name");
	
	// // console.log(samplePrompt);

	// console.log(typeof samplePrompt);

	// console.log("Hello, " + samplePrompt);

	function printWelcomeMessage(){
		let firstName = prompt("Enter your first name: ");
		let lastName = prompt("Enter your last name: ");

		console.log("Hello, " + firstName + " " + lastName + "!");
		console.log("Welcome to my page!");
	}

	printWelcomeMessage();




	

	// [SECTION] Function Naming Convention

	//  Function name should be definitive of the task to perform. It usually contains a verb

	function getCourses(){
		let courses = ["Science 101", "Math 101", "English 101"];
		console.log(courses)
	}
	getCourses();

	// Avoid generic names to avoid confusion within our code.

	function get(){
		let name = "Jamie";
		console.log(name);
	}
	get();

	// Avoid pointless and inappropriate function names, example: foo, bar, etc

		function foo(){
		console.log(25%5);
	}
	foo();

	// Avoid function in small caps. follow camelcase when naming variable anf functions

	function displayCarInfo(){
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1,500,00");
	}

	displayCarInfo();





